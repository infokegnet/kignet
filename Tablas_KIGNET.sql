-------------------------------------------TABLAS Y INSERT----------------------------------------------------------------
--CLIENTE

CREATE TABLE Cliente (
	id_cliente SERIAL,
	nom_empresa CHARACTER VARYING(50)  NOT NULL,
	nom_cliente CHARACTER VARYING(50)  NOT NULL,
	provincia CHARACTER VARYING(50)  NOT NULL,
	direccion CHARACTER VARYING(50)  NOT NULL,
	mail_cliente CHARACTER VARYING(50)  NOT NULL,
	telefono NUMERIC(9,0)  NOT NULL,
	
	CONSTRAINT pk_id_cliente PRIMARY KEY (id_cliente)
);

--CONSULTA

CREATE TABLE Consulta (
	id_consulta SERIAL,
	id_cliente SERIAL,
	servicio CHARACTER VARYING(50)  NOT NULL,
	info_adicional CHARACTER VARYING(500)  NOT NULL,
	estado CHARACTER VARYING(5)  NOT NULL,
	
	CONSTRAINT pk_id_consulta PRIMARY KEY (id_consulta),
	CONSTRAINT fk_id_cliente2 FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente)
);

--VALORACION

CREATE TABLE Valoracion (
	id_valor SERIAL,
	id_cliente SERIAL,
	nom_empresa CHARACTER VARYING(50) NOT NULL,
	opinion CHARACTER VARYING(500) NOT NULL,
	ventaja CHARACTER VARYING(500),
	desventaja CHARACTER VARYING(500),
	recomendacion CHARACTER VARYING(50)  NOT NULL,
	estrella NUMERIC(1,0) NOT NULL,
	
	CONSTRAINT pk_id_valor PRIMARY KEY (id_valor),
	CONSTRAINT fk_id_cliente FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente)
);

--DEPARTAMENTO

CREATE TABLE Departamento (
	id_dept SERIAL,
	nom_dept CHARACTER VARYING(50) NOT NULL,
	
	CONSTRAINT pk_id_dept PRIMARY KEY (id_dept)
);

--INSERT TABLA DEPARTAMENTO

INSERT INTO departamento(nom_dept)
VALUES('Gestión de Empresa');
INSERT INTO departamento(nom_dept)
VALUES('Gestión de Mantenimiento');
INSERT INTO departamento(nom_dept)
VALUES('Gestión de Web');

--Empleado

 CREATE TABLE empleado (
	id_emple SERIAL,
	id_dept SERIAL,
	nom_emple CHARACTER VARYING(50) NOT NULL,
	apellidos CHARACTER VARYING(50) NOT NULL,
	num_movil NUMERIC(9,0) NOT NULL,
	mail_emple CHARACTER VARYING(50) NOT NULL,
	fecha_nacimiento DATE NOT NULL,
	direccion CHARACTER VARYING(50) NOT NULL,
	pass_emple CHARACTER VARYING(50),
	
	CONSTRAINT pk_id_emple PRIMARY KEY (id_emple),
	CONSTRAINT fk_id_dept FOREIGN KEY (id_dept) REFERENCES departamento(id_dept)
);

--INSERT TABLA empleado

INSERT INTO empleado(nom_emple,apellidos,num_movil,mail_emple,fecha_nacimiento,direccion,pass_emple)
VALUES('Ingridi','De Freitas Silva',654789123,'ac19idefreitas@inslaferreria.cat','2000-07-09','Calle Sau',encode(digest('K&GN3T_INGRIDI', 'sha1'), 'hex'))RETURNING id_dept;
INSERT INTO empleado(nom_emple,apellidos,num_movil,mail_emple,fecha_nacimiento,direccion,pass_emple)
VALUES('Gabriel','Santos Clemente',654123789,'ac19gsantos@inslaferreria.cat','2000/11/16','Calle Sau',encode(digest('K&GN3T_GABRIEL', 'sha1'), 'hex'))RETURNING id_dept;
INSERT INTO empleado(nom_emple,apellidos,num_movil,mail_emple,fecha_nacimiento,direccion,pass_emple)
VALUES('Karla','Matinez Rodriguez',654781239,'ac19kmartinez@inslaferreria.cat','2000/06/24','Calle Pirineus',encode(digest('K&GN3T_KARLA', 'sha1'), 'hex'))RETURNING id_dept;
INSERT INTO empleado(nom_emple,apellidos,num_movil,mail_emple,fecha_nacimiento,direccion,pass_emple)
VALUES('Sarah','Matinez Silva Santos',655654123,'ac19smsilsan@inslaferreria.cat','2001/02/25','Calle Reus')RETURNING id_dept;


--SOLUCION

CREATE TABLE Solucion (
	id_solucion SERIAL,
	id_cliente SERIAL,
	id_consulta SERIAL,
	id_emple SERIAL,
	nombre_cliente CHARACTER VARYING(50)  NOT NULL,
	mail_empresa CHARACTER VARYING(50)  NOT NULL,
	mail_cliente CHARACTER VARYING(50)  NOT NULL,
	asunto CHARACTER VARYING(50)  NOT NULL,
	hora_solucion CHARACTER VARYING(50) NOT NULL,
	realizado_por CHARACTER VARYING(50) NOT NULL,
	
	CONSTRAINT pk_id_solucion PRIMARY KEY (id_solucion),
	CONSTRAINT fk_id_cliente3 FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
	CONSTRAINT fk_id_consulta FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta),
	CONSTRAINT fk_id_emple FOREIGN KEY (id_emple) REFERENCES empleado(id_emple)
);


-------------------------------------------AUDITORIA Y TABLA_AUDITORIA----------------------------------------------------------------
--TABLA

CREATE TABLE tbl_audit (
id_auditoria serial,
nombre_tabla character (50) NOT NULL, 
operacion character (10) NOT NULL,
valor_viejo text,
valor_nuevo text,
fecha_actual timestamp  without time zone NOT NULL, 
usuario character (50) NOT NULL,
CONSTRAINT id_auditoria_pk PRIMARY KEY (id_auditoria));

--FUNCTION
CREATE OR REPLACE FUNCTION auditoria () RETURNS trigger AS
$$
BEGIN 
IF (TG_OP =  'DELETE') THEN
	PERFORM dblink_connect ('port=5432 dbname=auditoria_kignet user=postgres password=root') ;
	PERFORM dblink_exec ('INSERT INTO tbl_audit (nombre_tabla, operacion, 
	valor_viejo, valor_nuevo, fecha_actual, usuario) VALUES  ('''||TG_TABLE_NAME|| ''',
	''' ||TG_OP|| ''' , ''' ||OLD|| ''' , NULL,  now () , USER);') ;
	PERFORM dblink_disconnect (); 
	RETURN OLD;
	END IF;

IF (TG_OP = 'UPDATE') THEN
	PERFORM dblink_connect ('port=5432 dbname=auditoria_kignet user=postgres password=root') ;
	PERFORM dblink_exec ('INSERT INTO tbl_audit (nombre_tabla,operacion, 
	valor_viejo, valor_nuevo, fecha_actual, usuario) VALUES  ('''|| TG_TABLE_NAME || ''',
	''' || TG_OP || ''' , ''' || OLD || ''' , ''' || NEW || ''',  now () , USER) ;') ;
	PERFORM dblink_disconnect (); 
	RETURN NEW;
	END IF;

IF (TG_OP = 'INSERT') THEN 
	PERFORM dblink_connect ('port=5432 dbname=auditoria_kignet user=postgres password=root') ;
	PERFORM dblink_exec ('INSERT INTO tbl_audit (nombre_tabla,operacion, 
	valor_viejo, valor_nuevo, fecha_actual, usuario) VALUES  ('''|| TG_TABLE_NAME || ''',
	''' || TG_OP || ''' , NULL , ''' || NEW || ''',  now () , USER) ;') ;
	PERFORM dblink_disconnect (); 
	RETURN NEW;
	END IF;
RETURN NULL;
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

--CONECTAR A LA BASE DE DATOS AUDITORIA
SELECT DBLINK_CONNECT('dbname=auditoria_kignet
user=postgres password=root');

--TRIGGER

CREATE TRIGGER trigger_auditoria
AFTER INSERT OR UPDATE OR DELETE ON empleado
FOR EACH ROW EXECUTE PROCEDURE auditoria();